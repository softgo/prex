package com.prex.auth.security;

import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @Classname PrexClientDetailsService
 * @Description 支持自定义
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-05 09:21
 * @Version 1.0
 */
public class PrexClientDetailsService extends JdbcClientDetailsService {

    private static final Map<String, ClientDetails> CLIENTS = new HashMap<>();

    public static void clearCache() {
        CLIENTS.clear();
    }
    public static void clearCache(String clientId) {
        CLIENTS.remove(clientId);
    }

    public PrexClientDetailsService(DataSource dataSource) {
        super(dataSource);
    }

    /**
     * 重写原生方法支持缓存
     *
     * @param clientId
     * @return
     * @throws InvalidClientException
     */
    @Override
    public ClientDetails loadClientByClientId(String clientId) throws InvalidClientException {
        ClientDetails clientDetails = CLIENTS.get(clientId);
        if (clientDetails != null) {
            return clientDetails;
        }
        ClientDetails clientDetails1 = super.loadClientByClientId(clientId);
        CLIENTS.put(clientId, clientDetails1);
        return clientDetails1;
    }
}
